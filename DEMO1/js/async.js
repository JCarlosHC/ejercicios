function lenta(){
    var i=0;
    while(i<10000) i++;
    console.log("Fin Lenta");
}

function sinEspera(){
    return new Promise(resolve => {
        var i=0;
        while(i<10000) i++;
        resolve("Fin espera");
    });
}

async function asyncCall(){
    await sinEspera()
        .then((result) => {
            console.log("Terminado: "+result);
        })
}